<?php

declare(strict_types=1);

namespace App\Charts;

use App\Contracts\Services\CurrencyServiceInterface;
use App\Services\Coindesk\Params\CurrencyParams;
use App\Services\Prediction\PredictionService;
use Carbon\Carbon;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class SampleChart extends BaseChart
{
    private CurrencyServiceInterface $currencyService;

    private PredictionService $predictionService;

    public function __construct(
        CurrencyServiceInterface $currencyService,
        PredictionService $predictionService
    ) {
        $this->currencyService = $currencyService;
        $this->predictionService = $predictionService;
    }

    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $currency = $request->get('currency', 'USD');

        $params = new CurrencyParams($currency, Carbon::now()->subDays(6), Carbon::now()->addDay());

        $result = $this->currencyService->getBtcHistory($params);
        $dates = $result->keys()->all();
        $prices = $result->values()->all();
        $tomorrow = Carbon::now()->addDay()->format('Y-m-d');

        $prediction = $this->predictionService->trainAndPredict($dates, $prices, [$tomorrow]);

        $dates[] = "$tomorrow(prediction)";
        $prices[] = (float)$prediction;

        return Chartisan::build()
            ->labels($dates)
            ->dataset('BTC Prices', $prices);
    }
}
