<?php declare(strict_types=1);

namespace App\Providers;

use App\Contracts\Services\CurrencyServiceClientInterface;
use App\Contracts\Services\CurrencyServiceInterface;
use App\Services\Coindesk\CoindeskServiceClient;
use App\Services\Coindesk\CoindeskService;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class CurrencyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CurrencyServiceInterface::class, CoindeskService::class);

        $this->app->bind(CurrencyServiceClientInterface::class, static function (Application $app) {
            return new CoindeskServiceClient($app->get('config')->get('services.coindesk'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
