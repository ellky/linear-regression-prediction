<?php declare(strict_types=1);

namespace App\Contracts\Services;

use App\Services\Coindesk\Contracts\Currencies\Crypto\CoindeskCryptoCurrencyInterface;
use App\Services\Coindesk\Contracts\CurrencyParamsInterface;

interface CurrencyServiceClientInterface
{
    /**
     * @param CoindeskCryptoCurrencyInterface $currency
     * @param CurrencyParamsInterface         $params
     *
     * @return mixed
     */
    public function getHistory(CoindeskCryptoCurrencyInterface $currency, CurrencyParamsInterface $params): array;
}
