<?php declare(strict_types=1);

namespace App\Contracts\Services;

use App\Services\Coindesk\Contracts\CurrencyParamsInterface;
use Illuminate\Support\Collection;

interface CurrencyServiceInterface
{
    /**
     * @param CurrencyParamsInterface $params
     *
     * @return Collection
     */
    public function getBtcHistory(CurrencyParamsInterface $params): Collection;
}
