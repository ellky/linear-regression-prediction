<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CurrencyHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currency' => 'nullable|string|size:3',
            'from' => 'nullable|string|date_format:Y-m-d|before:today',
            'to' => 'nullable|string|date_format:Y-m-d|after:from|before:tomorrow',
        ];
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->get('currency', 'USD');
    }

    /**
     * @return Carbon
     */
    public function getFrom(): Carbon
    {
        $from = $this->get('from');

        if ($from) {
            return Carbon::parse($from);
        }

        return Carbon::now()->subDays(7);
    }

    /**
     * @return Carbon
     */
    public function getAfter(): Carbon
    {
        $to = $this->get('to');

        if ($to) {
            return Carbon::parse($to);
        }

        return Carbon::now();
    }
}
