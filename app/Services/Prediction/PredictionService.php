<?php declare(strict_types=1);

namespace App\Services\Prediction;

use Phpml\Regression\SVR;
use Phpml\SupportVectorMachine\Kernel;
use Phpml\Exception\LibsvmCommandException;

class PredictionService
{
    /**
     * @param array $samples
     * @param array $targets
     * @param array $predict
     *
     * @return array|string
     * @throws LibsvmCommandException
     */
    public function trainAndPredict(array $samples, array $targets, array $predict = [])
    {
        $regression = $this->createRegression();

        $dates = $this->prepareSamples($samples);

        $regression->train($dates, $targets);

        return $regression->predict($predict);
    }

    /**
     * @param array $samples
     *
     * @return array
     */
    private function prepareSamples(array $samples): array
    {
        return array_map(static function ($date) {
            return [$date];
        }, $samples);
    }

    /**
     * @return SVR
     */
    private function createRegression(): SVR
    {
        return new SVR(Kernel::LINEAR);
    }
}

