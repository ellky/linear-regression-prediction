<?php declare(strict_types=1);

namespace App\Services\Coindesk\Contracts\Currencies\Crypto;

interface CoindeskCryptoCurrencyInterface
{
    /**
     * @return string
     */
    public function getIndex(): string;
}
