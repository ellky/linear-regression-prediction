<?php declare(strict_types=1);

namespace App\Services\Coindesk\Contracts;

use Carbon\Carbon;

interface CurrencyParamsInterface
{
    /**
     * @return string
     */
    public function getIso3CurrencyCode(): string;

    /**
     * @return Carbon|null
     */
    public function getStartDate(): ?Carbon;

    /**
     * @return Carbon|null
     */
    public function getEndDate(): ?Carbon;
}
