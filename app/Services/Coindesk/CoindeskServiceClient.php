<?php declare(strict_types=1);

namespace App\Services\Coindesk;

use App\Contracts\Services\CurrencyServiceClientInterface;
use App\Services\Coindesk\Builders\UriBuilder;
use App\Services\Coindesk\Contracts\Currencies\Crypto\CoindeskCryptoCurrencyInterface;
use App\Services\Coindesk\Contracts\CurrencyParamsInterface;
use Illuminate\Support\Facades\Http;

class CoindeskServiceClient implements CurrencyServiceClientInterface
{
    /**
     * @var mixed
     */
    private $url;

    /**
     * @var mixed
     */
    private $version;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        ['url' => $url, 'version' => $version] = $options;

        $this->url = $url;
        $this->version = $version;
    }

    /**
     * @inheritdoc
     */
    public function getHistory(CoindeskCryptoCurrencyInterface $currency, CurrencyParamsInterface $params): array
    {
        $index = $currency->getIndex();

        $builder = $this->createBuilder()
            ->addIndex($index)
            ->addHistory()
            ->addFile('close.json')
            ->addCurrency($params->getIso3CurrencyCode())
            ->addStartDate($params->getStartDate())
            ->addEndDate($params->getEndDate());

        $response = Http::get($builder->build());

        if ($response->successful()) {
            return $response->json()[$index];
        } else {
            abort(503);
        }
    }

    /**
     * @return UriBuilder
     */
    private function createBuilder(): UriBuilder
    {
        return new UriBuilder($this->url, $this->version);
    }
}
