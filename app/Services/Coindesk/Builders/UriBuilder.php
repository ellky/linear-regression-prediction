<?php declare(strict_types=1);

namespace App\Services\Coindesk\Builders;

use Carbon\Carbon;

class UriBuilder
{
    private const DATE_FORMAT = 'Y-m-d';
    private array $urlParts = [];
    private array $queryParts = [];

    /**
     * @param string $url
     * @param string $version
     */
    public function __construct(string $url, string $version)
    {
        $this->addUrlPart($url);
        $this->addUrlPart($version);
    }

    /**
     * @param string $index
     *
     * @return UriBuilder
     */
    public function addIndex(string $index): self
    {
        $this->addUrlPart($index);

        return $this;
    }

    /**
     * @return $this
     */
    public function addHistory(): self
    {
        $this->addUrlPart('historical');

        return $this;
    }

    /**
     * @param string $format
     *
     * @return $this
     */
    public function addFile(string $format): self
    {
        $this->addUrlPart($format);

        return $this;
    }

    /**
     * @param string $currency
     *
     * @return $this
     */
    public function addCurrency(string $currency): self
    {
        $this->addQueryPart("currency=$currency");

        return $this;
    }

    /**
     * @param Carbon|null $startDate
     *
     * @return $this
     */
    public function addStartDate(Carbon $startDate = null): self
    {
        if ($startDate) {
            $date = $startDate->format(self::DATE_FORMAT);

            $this->addQueryPart("start=$date");
        }

        return $this;
    }

    /**
     * @param Carbon|null $endDate
     *
     * @return $this
     */
    public function addEndDate(Carbon $endDate = null): self
    {
        if ($endDate) {
            $date = $endDate->format(self::DATE_FORMAT);

            $this->addQueryPart("end=$date");
        }

        return $this;
    }

    /**
     * @return string
     */
    public function build(): string
    {
        return implode('?', [$this->buildUrl(), $this->buildQuery()]);
    }

    /**
     * @return string
     */
    private function buildUrl(): string
    {
        return implode('/', $this->urlParts);
    }

    /**
     * @return string
     */
    private function buildQuery(): string
    {
        return implode('&', $this->queryParts);
    }

    /**
     * @param string $part
     */
    private function addQueryPart(string $part): void
    {
        array_push($this->queryParts, $part);
    }

    /**
     * @param string $part
     */
    private function addUrlPart(string $part): void
    {
        array_push($this->urlParts, $part);
    }
}
