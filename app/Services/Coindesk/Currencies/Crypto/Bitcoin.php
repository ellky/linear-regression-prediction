<?php declare(strict_types=1);

namespace App\Services\Coindesk\Currencies\Crypto;

use App\Services\Coindesk\Contracts\Currencies\Crypto\CoindeskCryptoCurrencyInterface;
use App\Services\Coindesk\Contracts\Currencies\Crypto\CryptoCurrencyInterface;

class Bitcoin implements CryptoCurrencyInterface, CoindeskCryptoCurrencyInterface
{
    /**
     * @inheritdoc
     */
    public function getIndex(): string
    {
        return 'bpi';
    }
}
