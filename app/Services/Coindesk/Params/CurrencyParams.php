<?php declare(strict_types=1);

namespace App\Services\Coindesk\Params;

use App\Services\Coindesk\Contracts\CurrencyParamsInterface;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\ParameterBag;

class CurrencyParams extends ParameterBag implements CurrencyParamsInterface
{
    private const ISO3_CURRENCY_KEY = 'iso3Currency';
    private const PERIOD_START_KEY = 'periodStart';
    private const PERIOD_END_KEY = 'periodEnd';

    /**
     * @param string      $iso3Currency
     * @param Carbon|null $start
     * @param Carbon|null $end
     */
    public function __construct(string $iso3Currency = 'USD', Carbon $start = null, Carbon $end = null)
    {
        parent::__construct([
            self::ISO3_CURRENCY_KEY => $iso3Currency,
            self::PERIOD_START_KEY => $start,
            self::PERIOD_END_KEY => $end,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getIso3CurrencyCode(): string
    {
        return $this->get(self::ISO3_CURRENCY_KEY);
    }

    /**
     * @inheritdoc
     */
    public function getStartDate(): ?Carbon
    {
        return $this->get(self::PERIOD_START_KEY);
    }

    /**
     * @inheritdoc
     */
    public function getEndDate(): ?Carbon
    {
        return $this->get(self::PERIOD_END_KEY);
    }
}
