<?php declare(strict_types=1);

namespace App\Services\Coindesk;

use App\Contracts\Services\CurrencyServiceClientInterface;
use App\Contracts\Services\CurrencyServiceInterface;
use App\Services\Coindesk\Contracts\CurrencyParamsInterface;
use App\Services\Coindesk\Currencies\Crypto\Bitcoin;
use Illuminate\Support\Collection;

class CoindeskService implements CurrencyServiceInterface
{
    private CurrencyServiceClientInterface $client;

    /**
     * @param CurrencyServiceClientInterface $client
     */
    public function __construct(CurrencyServiceClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritdoc
     */
    public function getBtcHistory(CurrencyParamsInterface $params): Collection
    {
        $result = $this->client->getHistory(new Bitcoin, $params);

        return new Collection($result);
    }
}
