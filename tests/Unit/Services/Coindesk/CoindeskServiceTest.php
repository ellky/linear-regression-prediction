<?php declare(strict_types=1);

namespace tests\Unit\Services\Coindesk;

use App\Contracts\Services\CurrencyServiceClientInterface;
use App\Contracts\Services\CurrencyServiceInterface;
use App\Services\Coindesk\CoindeskService;
use App\Services\Coindesk\Contracts\CurrencyParamsInterface;
use App\Services\Coindesk\Currencies\Crypto\Bitcoin;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class CoindeskServiceTest extends TestCase
{

    /**
     * @var CurrencyServiceInterface|MockObject
     */
    private $client;

    /**
     * @var CurrencyParamsInterface|MockObject
     */
    private $params;

    protected function setUp(): void
    {
        $this->client = $this->createMock(CurrencyServiceClientInterface::class);
        $this->params = $this->createMock(CurrencyParamsInterface::class);
    }

    public function getBtcHistoryDataProvider(): array
    {
        return [
            'case when we retrieve list of prices for BTC based in some currency' => [
                'data' => [
                    'result' => [
                        'some result is here',
                    ],
                ],
                'expected' => [
                    'some result is here',
                ],
            ],
        ];
    }

    /**
     * @dataProvider getBtcHistoryDataProvider
     *
     * @param array $data
     * @param       $expected
     *
     * @small
     */
    public function testGetBtcHistory(array $data, $expected): void
    {
        ['result' => $result] = $data;

        $instance = $this->creteInstance();

        $this->client->expects($this->once())
            ->method('getHistory')
            ->with(new Bitcoin, $this->params)
            ->willReturn($result);

        $collection = $instance->getBtcHistory($this->params);

        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertEquals($expected, $collection->all());
    }

    /**
     * @return CoindeskService
     */
    private function creteInstance(): CoindeskService
    {
        return new CoindeskService($this->client);
    }
}
