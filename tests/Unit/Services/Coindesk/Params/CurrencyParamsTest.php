<?php declare(strict_types=1);

namespace tests\Unit\Services\Coindesk\Params;

use App\Services\Coindesk\Params\CurrencyParams;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class CurrencyParamsTest extends TestCase
{
    public function getIso3CurrencyCodeDataProvider(): array
    {
        return [
            'case when we provide currency iso code' => [
                'data' => [
                    'currency' => 'USD',
                ],
                'expected' => 'USD',
            ],
        ];
    }

    public function getStartDateDataProvider(): array
    {
        $start = Carbon::now();

        return [
            'case when we provide start date' => [
                'data' => [
                    'currency' => 'USD',
                    'startDate' => $start,
                ],
                'expected' => $start,
            ],
            'case when we doesn\'t provide start date' => [
                'data' => [
                    'currency' => 'USD',
                    'startDate' => null,
                ],
                'expected' => null,
            ],
        ];
    }

    public function getEndDateDataProvider(): array
    {
        $endDate = Carbon::now()->subDays(1);

        return [
            'case when we provide end date' => [
                'data' => [
                    'currency' => 'USD',
                    'startDate' => Carbon::now(),
                    'endDate' => $endDate,
                ],
                'expected' => $endDate,
            ],
            'case when we doesn\'t provide end date' => [
                'data' => [
                    'currency' => 'USD',
                    'startDate' => Carbon::now(),
                    'endDate' => null,
                ],
                'expected' => null,
            ],
        ];
    }

    /**
     * @dataProvider getIso3CurrencyCodeDataProvider
     *
     * @param array $data
     * @param       $expected
     *
     * @small
     */
    public function testGetIso3CurrencyCode(array $data, $expected): void
    {
        ['currency' => $currency] = $data;

        $instance = $this->createInstance($currency);

        $this->assertEquals($expected, $instance->getIso3CurrencyCode());
    }

    /**
     * @dataProvider getStartDateDataProvider
     *
     * @param array $data
     * @param       $expected
     *
     * @small
     */
    public function testGetStartDate(array $data, $expected): void
    {
        ['currency' => $currency, 'startDate' => $startDate] = $data;

        $instance = $this->createInstance($currency, $startDate);

        $this->assertEquals($expected, $instance->getStartDate());
    }

    /**
     * @dataProvider getEndDateDataProvider
     *
     * @param array $data
     * @param       $expected
     *
     * @small
     */
    public function testGetEndDate(array $data, $expected): void
    {
        ['currency' => $currency, 'startDate' => $startDate, 'endDate' => $endDate] = $data;

        $instance = $this->createInstance($currency, $startDate, $endDate);

        $this->assertEquals($expected, $instance->getEndDate());
    }

    /**
     * @param string      $currency
     * @param Carbon|null $startDate
     * @param Carbon|null $endDate
     *
     * @return CurrencyParams
     */
    private function createInstance(string $currency, Carbon $startDate = null, Carbon $endDate = null): CurrencyParams
    {
        return new CurrencyParams($currency, $startDate, $endDate);
    }
}
