<?php declare(strict_types=1);

namespace tests\unit\Services\Coindesk;

use App\Services\Coindesk\CoindeskServiceClient;
use App\Services\Coindesk\Contracts\Currencies\Crypto\CoindeskCryptoCurrencyInterface;
use App\Services\Coindesk\Contracts\CurrencyParamsInterface;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class CoindeskServiceClientTest extends TestCase
{
    /**
     * @var CoindeskCryptoCurrencyInterface|MockObject
     */
    private $coindeskCryptoCurrency;

    /**
     * @var CurrencyParamsInterface|MockObject
     */
    private $params;

    protected function setUp(): void
    {
        $this->coindeskCryptoCurrency = $this->createMock(CoindeskCryptoCurrencyInterface::class);
        $this->params = $this->createMock(CurrencyParamsInterface::class);
    }

    public function getCurrencyHistoryDataProvider(): array
    {
        return [
            'case when we provide right data' => [
                'data' => [
                    'options' => [
                        'url' => 'some/url',
                        'version' => 'v2',
                    ],
                ],
                'expected' => [],
            ],
        ];
    }

    /**
     * @dataProvider getCurrencyHistoryDataProvider
     *
     * @param array $data
     * @param       $expected
     *
     * @small
     */
    public function testGetCurrencyHistory(array $data, $expected): void
    {
        ['options' => $options] = $data;

        $instance = $this->createInstance($options);

        $this->assertEquals($expected, $instance->getHistory($this->coindeskCryptoCurrency, $this->params));
    }

    /**
     * @param array $options
     *
     * @return CoindeskServiceClient
     */
    private function createInstance(array $options): CoindeskServiceClient
    {
        return new CoindeskServiceClient($options);
    }
}
